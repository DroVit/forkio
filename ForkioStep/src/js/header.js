// const navMenu = document.querySelector('.nav__menu');
// const cross = document.querySelector('.nav__cross');
// const burger = document.querySelector('.nav__btnMenu');

// burger.addEventListener('click', () => {
//     if (!burger.classList.contains('hide')) {
//         burger.classList.add('hide');
//         cross.classList.remove('hide');
//         navMenu.classList.remove('hide');
//     }
// })
// cross.addEventListener('click', () => {
//     if (!cross.classList.contains('hide')) {
//         cross.classList.add('hide');
//         navMenu.classList.add('hide');
//         burger.classList.remove('hide');
//     }
// })
const open = document.querySelector('.header__nav');
const cross = document.querySelector('.nav__cross');
const burger = document.querySelector('.nav__btnMenu');

burger.addEventListener('click', () => {
    open.classList.toggle('openMenu');
});

cross.addEventListener('click', () => {
    open.classList.toggle('openMenu');
});